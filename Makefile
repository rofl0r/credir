# if you want to change/override some variables, do so in a file called
# config.mak, which is gets included automatically if it exists.

prefix = /usr/local
bindir = $(prefix)/bin

PROG = credir
SRCS =  sockssrv.c server.c sblist.c sblist_delete.c randombytes.c

LIBS = -lpthread

CFLAGS += -Wall -std=c99 -Wno-unknown-pragmas
CFLAGS += -Drandombytes=randombytes_l

ifeq ($(USE_SODIUM),1)
CFLAGS += -DUSE_SODIUM
LIBS += -lsodium
else
SRCS += tweetnacl.c
endif

OBJS = $(SRCS:.c=.o)

-include config.mak

all: $(PROG)

install: $(PROG)
	install -d $(DESTDIR)/$(bindir)
	install -D -m 755 $(PROG) $(DESTDIR)/$(bindir)/$(PROG)

clean:
	rm -f $(PROG)
	rm -f $(OBJS)

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $(INC) $(PIC) -c -o $@ $<

$(PROG): $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) $(LIBS) -o $@

credir-keygen: credir-keygen.c tweetnacl.o randombytes.o
	$(CC) $(LDFLAGS) $^ $(LIBS) -o $@

.PHONY: all clean install

