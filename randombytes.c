#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

typedef unsigned char u8;
typedef unsigned long u32;
typedef unsigned long long u64;

#ifdef HAVE_GETRANDOM
#include <sys/random.h>
void randombytes_l(u8 * buf, u64 len) {
	ssize_t left = len;
	u8 *dst = buf;
	while(left) {
		ssize_t nr = getrandom(dst, left, 0);
		if(nr == -1) {
			if(errno == EINTR) continue;
			abort();
		}
		dst += nr;
		left -= nr;
	}
}
#else
void randombytes_l(u8 * buf, u64 len) {
	int fd = open("/dev/urandom", O_RDONLY);
	if (fd == -1) fd = open("/dev/random", O_RDONLY);
	if (fd == -1) abort();
	ssize_t left = len;
	u8 *dst = buf;
	while(left) {
		ssize_t nr = read(fd, dst, left);
		if(nr == -1) {
			if(errno == EINTR) continue;
			abort();
		}
		dst += nr;
		left -= nr;
	}
	close(fd);
}
#endif

