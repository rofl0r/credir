#include "tweetnacl.h"
#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

static int usage(char *a0) {
	fprintf(stderr, "%s [-s] filename\n\n"
	"creates public key pair for use with credir.\n"
	"filename suffix .priv and .pub will be added\n"
	"automatically.\n\n"
	"options: use -s if you create keys for the server,\n"
	"         as they are of different type/size.\n"
	, a0);
	return 1;
}

#ifndef MAX
#define MAX(a, b) ((a > b) ? a : b)
#endif

static unsigned char pk[MAX(
	crypto_box_curve25519xsalsa20poly1305_PUBLICKEYBYTES,
	crypto_sign_ed25519_PUBLICKEYBYTES)];
static unsigned char sk[MAX(
	crypto_box_curve25519xsalsa20poly1305_SECRETKEYBYTES,
	crypto_sign_ed25519_SECRETKEYBYTES)];

static int server_mode;

void raw2hex(char *dst, const unsigned char *src, size_t len) {
	size_t i, o;
	static const char hextab[] = "0123456789abcdef";
	for (i = 0, o=0; i < len; i++, o+=2) {
		dst[o + 0] = hextab[0xf & (src[i] >> 4)];
		dst[o + 1] = hextab[0xf & src[i]];
	}
	dst[o] = 0;
}

static void write_keys(char *filename_prefix) {
	char buf[512];
	const struct fmap {
		char ext[5];
		unsigned char *dest;
		unsigned len;
		int mode;
	} map[] = {
		[0] = {.ext = "pub", .dest = pk, .mode = 0644,
		       .len = server_mode ? crypto_sign_ed25519_PUBLICKEYBYTES : crypto_box_curve25519xsalsa20poly1305_PUBLICKEYBYTES},
		[1] = {.ext = "priv", .dest = sk, .mode = 0600,
		       .len = server_mode ? crypto_sign_ed25519_SECRETKEYBYTES : crypto_box_curve25519xsalsa20poly1305_SECRETKEYBYTES},
		[2] = {.len = 0}
	};
	const struct fmap * curr;
	for(curr = &map[0]; curr->len; ++curr) {
		snprintf(buf, sizeof buf, "%s.%s", filename_prefix, curr->ext);
		int fd = open(buf, O_CREAT | O_WRONLY | O_TRUNC, curr->mode);
		FILE *f = 0;
		if(fd != -1) f = fdopen(fd, "w");
		if(!f) {
			dprintf(2, "error opening %s\n", buf);
			exit(1);
		}
		raw2hex(buf, curr->dest, curr->len);
		if(curr->len*2 != fwrite(buf, 1, curr->len*2, f)) {
			dprintf(2, "error: short write to keyfile\n");
			exit(1);
		}
		fclose(f);
	}
}

int main(int argc, char** argv) {
	int c;
	while((c = getopt(argc, argv, "s")) != EOF) switch(c) {
		case 's': server_mode = 1; break;
		default: return usage(argv[0]);
	}
	if(optind == argc) return usage(argv[0]);
	char *fname = argv[optind];
	if(server_mode)
		crypto_sign_ed25519_keypair(pk, sk);
	else
		crypto_box_curve25519xsalsa20poly1305_keypair(pk, sk);
	write_keys(fname);
	return 0;
}
