/*
   CRedir - an encrypted port redirector.

   Copyright (C) 2019 rofl0r.

*/

#define VERSION_MAJOR 2
#define VERSION_MINOR 0

#define _GNU_SOURCE
#include <unistd.h>
#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <poll.h>
#include <arpa/inet.h>
#include <errno.h>
#include <limits.h>
#include <fcntl.h>
#include <assert.h>
#include "server.h"
#include "sblist.h"
#ifdef USE_SODIUM
#include <sodium.h>
#else
#include "tweetnacl.h"
void randombytes(unsigned char* buf, unsigned long long len);
#endif

#ifndef MAX
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#endif

#ifdef PTHREAD_STACK_MIN
#define THREAD_STACK_SIZE MAX(8*1024, PTHREAD_STACK_MIN)
#else
#define THREAD_STACK_SIZE 64*1024
#endif

#if defined(__APPLE__)
#undef THREAD_STACK_SIZE
#define THREAD_STACK_SIZE 64*1024
#elif defined(__GLIBC__) || defined(__FreeBSD__)
#undef THREAD_STACK_SIZE
#define THREAD_STACK_SIZE 32*1024
#endif

/* define some shortcuts */
#define PUBLICKEYBYTES crypto_box_curve25519xsalsa20poly1305_PUBLICKEYBYTES
#define SECRETKEYBYTES crypto_box_curve25519xsalsa20poly1305_SECRETKEYBYTES
#define SIGNPUBLICKEYBYTES crypto_sign_ed25519_PUBLICKEYBYTES
#define SIGNSECRETKEYBYTES crypto_sign_ed25519_SECRETKEYBYTES
#define NONCEBYTES crypto_box_curve25519xsalsa20poly1305_NONCEBYTES
#define ZEROBYTES crypto_box_curve25519xsalsa20poly1305_ZEROBYTES
#define BOXZEROBYTES crypto_box_curve25519xsalsa20poly1305_BOXZEROBYTES
#define KEYBYTES crypto_stream_xsalsa20_KEYBYTES
#define SIGNBYTES crypto_sign_ed25519_BYTES

static const struct server* server;
static int timeout;
static int client_mode;
static int verbose;

struct ecc_keys {
	unsigned char pubkey[PUBLICKEYBYTES];
	unsigned char privkey[SECRETKEYBYTES];
};

struct sign_keys {
	unsigned char pubkey[SIGNPUBLICKEYBYTES];
	unsigned char privkey[SIGNSECRETKEYBYTES];
};

union static_keys {
	struct sign_keys server;
	struct ecc_keys client;
};

static union static_keys static_keys;

static sblist *authorized_keys;

static struct target {
	union sockaddr_union addr;
	union sockaddr_union bind_addr;
} target;

struct thread {
	pthread_t pt;
	struct client client;
	unsigned char session_key[KEYBYTES];
	volatile int done;
};

#ifndef CONFIG_LOG
#define CONFIG_LOG 1
#endif
#if CONFIG_LOG
/* we log to stderr because it's not using line buffering, i.e. malloc which would need
   locking when called from different threads. for the same reason we use dprintf,
   which writes directly to an fd. */
#define dolog(...) dprintf(2, __VA_ARGS__)
#else
static void dolog(const char* fmt, ...) { (void) fmt; }
#endif

static inline int myisascii(int x) { return x >= ' ' && x < 127; }
static void dump(const unsigned char* data, size_t len) {
	static const char atab[] = "0123456789abcdef";
	char hex[24*2+1], ascii[24+1];
	unsigned h = 0, a = 0;
	int fill = ' ';

	while(len) {
		len--;
		hex[h++] = atab[*data >> 4];
		hex[h++] = atab[*data & 0xf];
		ascii[a++] = myisascii(*data) ? *data : '.';
		if(a == 24) {
	dump:
			hex[h] = 0;
			ascii[a] = 0;
			printf("%s\t%s\n", hex, ascii);

			if(fill == '_') return; /* jump from filler */

			a = 0;
			h = 0;
		}
		data++;
	}
	if(a) {
	filler:
		while(a<24) {
			hex[h++] = fill;
			hex[h++] = fill;
			ascii[a++] = fill;
		}
		goto dump;
	}
	a = 0;
	fill = '_';
	goto filler;
}
#define DUMP(P, L) do { if(verbose >= 2) dump(P, L); } while(0)
#define DEBUGP(...) do { if(verbose >= 1) dolog(__VA_ARGS__); } while(0)
#define DEBUGP2(...) do { if(verbose >= 2) dolog(__VA_ARGS__); } while(0)

typedef ssize_t (*iofunc)(int fildes, void *buf, size_t nbyte);
static ssize_t io_n(int fd, void* buf, size_t nb, iofunc io) {
	char *p = buf;
	size_t done = 0;
	while(done < nb) {
		ssize_t n = io(fd, p+done, nb-done);
		if(n < 0) return n;
		else if(n == 0) return done;
		done += n;
	}
	return done;
}

static ssize_t read_n(int fd, void* buf, size_t nb) {
	return io_n(fd, buf, nb, read);
}
static ssize_t write_n(int fd, void* buf, size_t nb) {
	return io_n(fd, buf, nb, (void*)write);
}

static int is_authorized(unsigned char* pubkey) {
	size_t i;
	/* if no authorized keys were specified, anyone is allowed access */
	if (sblist_getsize(authorized_keys) == 0) return 1;
	for(i=0; i<sblist_getsize(authorized_keys); ++i) {
		unsigned char *k = sblist_get(authorized_keys, i);
		if(!memcmp(pubkey, k, PUBLICKEYBYTES)) return 1;
	}
	return 0;
}

struct handshake_header {
	char sig[4];
	char version;
	char pad[3];
	/* server's pubkey for authentication/signing */
	unsigned char sign_pubkey[SIGNPUBLICKEYBYTES];
	/* server's signed ephemeral ecc pub key for this session */
	unsigned char eph_pubkey[PUBLICKEYBYTES+SIGNBYTES];
	/* challenge to be encrypted by the client, to proof its authenticity */
	unsigned char challenge[NONCEBYTES];
	/* nonce to be used for the public key crypto step */
	unsigned char nonce[NONCEBYTES];
};

struct handshake_client_reply {
	/* client's pubkey */
	unsigned char pubkey[PUBLICKEYBYTES];
	/* contains padding, the crypted challenge nonce, and the crypted salsa session key */
	unsigned char crypted[ZEROBYTES - BOXZEROBYTES + NONCEBYTES + KEYBYTES];
};

static int handshake(int fd, unsigned char *session_key, struct ecc_keys *ecc_keys) {
	ssize_t n;
	struct handshake_header hsh;
	struct handshake_client_reply hsc;
	if(client_mode) {
		n  = read_n(fd, &hsh, sizeof(hsh));
		if(n != sizeof(hsh)) {
er:			if(n < 0) DEBUGP("[%d] handshake read: %m\n", fd);
			else DEBUGP("[%d] handshake read %d\n", (int) n);
			return 0;
		}
		/* check server authencity and extract ephemeral public key */
		unsigned char server_ecc_pubkey[sizeof(hsh.eph_pubkey)];
		unsigned long long mlen = sizeof(server_ecc_pubkey);
		int ret = crypto_sign_open(server_ecc_pubkey, &mlen, hsh.eph_pubkey, sizeof(hsh.eph_pubkey), hsh.sign_pubkey);
		if(ret != 0 || !is_authorized(hsh.sign_pubkey)) {
			dolog("server failed authentication\n");
			return 0;
		}
		assert(mlen == PUBLICKEYBYTES);

		randombytes(session_key, KEYBYTES);
		unsigned char temp_crypted[ZEROBYTES + NONCEBYTES + KEYBYTES];
		unsigned char padded[sizeof(temp_crypted)], *padp = padded;
		memset(padp, 0, ZEROBYTES);
		padp += ZEROBYTES;
		memcpy(padp, hsh.challenge, sizeof(hsh.challenge));
		padp += sizeof(hsh.challenge);
		memcpy(padp, session_key, KEYBYTES);
		if(0 != crypto_box_curve25519xsalsa20poly1305(temp_crypted, padded, sizeof(padded), hsh.nonce, server_ecc_pubkey, ecc_keys->privkey)) {
			DEBUGP("[%d] handshake crypto_box failed!\n", fd);
			return 0;
		}
		memcpy(hsc.crypted, temp_crypted + BOXZEROBYTES, sizeof(hsc.crypted));
		memcpy(hsc.pubkey, ecc_keys->pubkey, sizeof hsc.pubkey);
		n = write_n(fd, &hsc, sizeof(hsc));
		if(n != sizeof(hsc)) {
ew:			if(n < 0) DEBUGP("[%d] handshake write: %m\n", fd);
			else DEBUGP("[%d] handshake write %d\n", (int) n);
			return 0;
		}
	} else {
		memcpy(hsh.sig, "\x53\x31\x33\x67", 4);
		hsh.version = 104;
		memcpy(hsh.pad, "\x33\x69\x31", 3);
		randombytes(hsh.challenge, sizeof(hsh.challenge));
		randombytes(hsh.nonce, sizeof(hsh.nonce));
		unsigned long long smlen = sizeof(hsh.eph_pubkey);
		crypto_sign(hsh.eph_pubkey, &smlen, ecc_keys->pubkey, sizeof(ecc_keys->pubkey), static_keys.server.privkey);
		assert(smlen == sizeof(hsh.eph_pubkey));
		memcpy(hsh.sign_pubkey, static_keys.server.pubkey, sizeof(hsh.sign_pubkey));
		n = write_n(fd, &hsh, sizeof(hsh));
		if(n != sizeof(hsh)) goto ew;
		n  = read_n(fd, &hsc, sizeof(hsc));
		if(n != sizeof(hsc)) goto er;
		unsigned char crypted_padded[BOXZEROBYTES + sizeof(hsc.crypted)];
		unsigned char decrypted[sizeof(crypted_padded)];
		memset(crypted_padded, 0, BOXZEROBYTES);
		memcpy(crypted_padded + BOXZEROBYTES, hsc.crypted, sizeof(hsc.crypted));
		int ret = crypto_box_curve25519xsalsa20poly1305_open(decrypted, crypted_padded, sizeof(crypted_padded), hsh.nonce, hsc.pubkey, ecc_keys->privkey);
		if(ret != 0 || !is_authorized(hsc.pubkey) || memcmp(decrypted+ZEROBYTES, hsh.challenge, sizeof(hsh.challenge)))
			return 0;
		memcpy(session_key, decrypted+ZEROBYTES+sizeof(hsh.challenge), KEYBYTES);
	}
	return 1;
}

static int connect_target(struct thread *t) {
	struct client *client = &t->client;
	const char* culprit = "socket";
	int af = SOCKADDR_UNION_AF(&target.addr),
	fd = socket(af, SOCK_STREAM, 0);
	if(fd == -1) {
		eval_errno: ;
		if(fd != -1) close(fd);
		int e = errno;
		switch(e) {
			case EPROTOTYPE:
			case EPROTONOSUPPORT:
			case EAFNOSUPPORT:
			case ECONNREFUSED:
			case ENETDOWN:
			case ENETUNREACH:
			case EHOSTUNREACH:
			case ETIMEDOUT:
			case EBADF:
			default:
			perror(culprit);
			return -1;
		}
	}
	if(SOCKADDR_UNION_AF(&target.bind_addr) != AF_UNSPEC &&
	   bindtoip(fd, &target.bind_addr) == -1)
		{ culprit = "bind"; goto eval_errno; }

	int flags = fcntl(fd, F_GETFL);
	if(fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
		err_fcntl:
		close(fd);
		perror("fcntl");
		return -1;
	}

	if(connect(fd, (void*)&target.addr, SOCKADDR_UNION_LENGTH(&target.addr)) == -1) {
		int e = errno;
		if (!(e == EINPROGRESS || e == EWOULDBLOCK))
			{ culprit = "connect"; goto eval_errno; }
	}

	if(fcntl(fd, F_SETFL, flags) == -1)
		goto err_fcntl;

	struct pollfd fds = {.fd = fd, .events = POLLOUT };
	int optval, ret;
	socklen_t optlen = sizeof(optval);

	ret = poll(&fds, 1, timeout ? timeout*1000 : -1);
	culprit = "poll";
	if(ret == 1 && (fds.revents & POLLOUT)) {
		ret = getsockopt(fd, SOL_SOCKET, SO_ERROR, &optval, &optlen);
		if(ret == -1) { culprit = "getsockopt"; goto eval_errno; }
		else if(optval) {
			errno = optval;
			goto eval_errno;
		}
	} else if(ret == 0) {
		errno = ETIMEDOUT;
		goto eval_errno;
	}
	DEBUGP2("connect succeeded\n");

	if(client_mode) {
		if(!handshake(fd, t->session_key, &static_keys.client)) {
			dolog("handshake failed\n");
			errno = ECONNREFUSED;
			culprit = "handshake";
			goto eval_errno;
		}
	}

	if(CONFIG_LOG) {
		char clientname[256], servname[256];
		int af;
		void *ipdata;
		af = SOCKADDR_UNION_AF(&client->addr);
		ipdata = SOCKADDR_UNION_ADDRESS(&client->addr);
		inet_ntop(af, ipdata, clientname, sizeof clientname);

		af = SOCKADDR_UNION_AF(&target.addr);
		ipdata = SOCKADDR_UNION_ADDRESS(&target.addr);
		inet_ntop(af, ipdata, servname, sizeof servname);
		dolog("client[%d] %s: connected to %s:%d\n", client->fd, clientname, servname, htons(SOCKADDR_UNION_PORT(&target.addr)));
	}
	return fd;
}

static void increase_nonce(unsigned char* nonce, size_t len) {
	size_t i;
	for(i=0; i<len && nonce[i] == 0xff; i++);
	if(i < len) ++nonce[i];
	memset(nonce, 0, i);
}

#ifdef CRC_CHECK
#include "crc32c.c"
static void calc_crc(unsigned char dest[4], void* buf, size_t len) {
	CRC32C_CTX cc;
	CRC32C_Init(&cc);
	CRC32C_Update(&cc, buf, len);
	CRC32C_Final(dest, &cc);
}
#else
#define calc_crc(A, B, C) do{}while(0)
#define CRC32C_InitTables() do{}while(0)
#endif

static void copyloop(int fd1, int fd2, unsigned char *session_key) {
	unsigned char client_nonce[crypto_stream_xsalsa20_NONCEBYTES] = {0};
	unsigned char server_nonce[crypto_stream_xsalsa20_NONCEBYTES] = {0};
	memset(client_nonce, 0xff, sizeof(client_nonce)/2);

	struct crypt_packet_header {
		unsigned short plen;
#ifdef CRC_CHECK
		unsigned char crc[4];
#endif
	} cph;

	struct pollfd fds[2] = {
		[0] = {.fd = fd1, .events = POLLIN},
		[1] = {.fd = fd2, .events = POLLIN},
	};

	while(1) {
		/* inactive connections are reaped after 15 min to free resources.
		   usually programs send keep-alive packets so this should only happen
		   when a connection is really unused. */
		switch(poll(fds, 2, 60*15*1000)) {
			case 0:
				/* this shouldn't ever happen */
				DEBUGP("both fds zero\n");
				return;
			case -1:
				if(errno == EINTR || errno == EAGAIN) continue;
				else perror("poll");
				return;
		}
		int infd = (fds[0].revents & POLLIN) ? fd1 : fd2;
		int outfd = infd == fd2 ? fd1 : fd2;
		unsigned char buf[1024];
		unsigned char crbuf[sizeof(cph)+sizeof(buf)];
		unsigned char *ptr;
		unsigned bl;
		// infd == fd1: clientfd
		// if clientfd and client_mode: read unenc, write enc
		// if !clientfd and client_mode: read enc, write unenc
		// if clientfd and !client_mode: read enc, write unenc
		// if !clientfd and !client_mode: read unenc, write enc
		int plain = (infd == fd1 && client_mode) || (infd != fd1 && !client_mode);
		unsigned char *nonce;
		ssize_t n;
		if(!plain) {
			/* read encrypted packet */
			nonce = client_mode ? server_nonce : client_nonce;
			increase_nonce(nonce, sizeof(server_nonce));

			bl = sizeof(cph);
			n = read_n(infd, &cph, bl);
			if(n != bl) {
er:				if(n < 0) DEBUGP("[%d%s] copyloop: read error %m\n", infd, plain?"p":"c");
				else if(n == 0) DEBUGP("[%d] copyloop: connection terminated\n", infd);
				else DEBUGP("[%d%s] copyloop: read error %d/%d\n", infd, plain?"p":"c", (int) n, (int) bl);
				return;
			}
			cph.plen = ntohs(cph.plen);
			if(cph.plen > sizeof(crbuf)) {
				DEBUGP("[%d] copyloop: received invalid packet length %u\n", fd1, (unsigned) cph.plen);
				return;
			}

			n = read_n(infd, crbuf, cph.plen);
			if(n != cph.plen) goto er;
			crypto_stream_xsalsa20_xor(buf, crbuf, n, nonce, session_key);
			DEBUGP("[%d] decrypted %d bytes\n", infd, (int) n);
			DUMP(buf, n < 48 ? n : 48);
#ifdef CRC_CHECK
			unsigned char crc[4];
			calc_crc(crc, buf, n);
			if(memcmp(crc, cph.crc, sizeof(cph.crc))) {
				dolog("checksum error!\n");
				return;
			}
#endif
			ptr = buf;

		} else {
			/* read plain packet, encrypt it */

			nonce = client_mode ? client_nonce : server_nonce;
			increase_nonce(nonce, sizeof(server_nonce));

			bl = sizeof(buf);
			n = read(infd, buf, bl);
			if(n <= 0) goto er;

			DEBUGP("[%d] received %d bytes\n", infd, (int) n);
			DUMP(buf, n < 48 ? n : 48);

			cph.plen = htons(n);
			calc_crc(cph.crc, buf, n);
			memcpy(crbuf, &cph, sizeof(cph));
			crypto_stream_xsalsa20_xor(crbuf+sizeof(cph), buf, n, nonce, session_key);
			ptr = crbuf;
			n += sizeof(cph);
		}
		ssize_t m = write_n(outfd, ptr, n);
		if(m != n) {
			if(m < 0) DEBUGP("[%d] copyloop: write error %m", fd1);
			else DEBUGP("[%d] copyloop: write error %d/%d", fd1, (int) m, (int) n);
			return;
		}
	}
}

static void* clientthread(void *data) {
	struct thread *t = data;
	struct ecc_keys eph_keys;
	if(!client_mode) {
		crypto_box_curve25519xsalsa20poly1305_keypair(eph_keys.pubkey, eph_keys.privkey);
		if(!handshake(t->client.fd, t->session_key, &eph_keys)) {
			dolog("client handshake failed\n");
			goto clean;
		}
	}
	int fd = connect_target(t);
	if(fd != -1) {
		copyloop(t->client.fd, fd, t->session_key);
		close(fd);
	}
clean:
	close(t->client.fd);
	t->done = 1;
	return 0;
}

static void collect(sblist *threads) {
	size_t i;
	for(i=0;i<sblist_getsize(threads);) {
		struct thread* thread = *((struct thread**)sblist_get(threads, i));
		if(thread->done) {
			pthread_join(thread->pt, 0);
			sblist_delete(threads, i);
			free(thread);
		} else
			i++;
	}
}

static int complain_bind(const char* addr) {
	dolog("error: the supplied bind address %s could not be resolved\n", addr);
	return 1;
}

static int usage(void) {
	dolog(
		"CRedir %d.%d - port redirector with encryption\n"
		"--------------------------------------------\n"
		"usage: credir -k key [-a authkeys -c -i listenip -p port -t timeout -b bindaddr -v] ip:port\n\n"
		"all arguments except -k are optional.\n"
		"if -c is given (clientmode), outgoing connections to ip/port will be encrypted.\n"
		"if -c isn't given (servermode), incoming connections will be decrypted and forwarded.\n\n"
		"-a a single or colon-separated list of authorized public keys encoded as hex.\n"
		"   if used, only the keys listed are granted access.\n"
		"   this assures on the client side that the server isn't mitm'd, an on\n"
		"   the server side that only known clients can use the service.\n"
		"   if none are specified, anyone can access.\n\n"
		"-k takes the filename prefix of a public key pair generated with credir-keygen\n"
		"   e.g. foo, if a file foo.priv and foo.pub were generated\n\n"
		"-b specifies the default ip outgoing connections are bound to.\n"
		"-t timeout is specified in seconds, default: %d.\n"
		"   if timeout is set to 0, block until the OS cancels the conn. attempt.\n\n"
		"-v enable verbose/debug output. can be passed multiple times to increase.\n"
		"\n"
		"by default listenip is 0.0.0.0 and port 1080.\n\n"
		"all incoming connections will be redirected to ip:port.\n"
		, VERSION_MAJOR, VERSION_MINOR, timeout
	);
	return 1;
}

static void dehex(unsigned char *dst, const char *src, unsigned len) {
	static const char hextab[] = "0123456789abcdef";
#define hval(X) (unsigned long)(strchr(hextab, X) - hextab)
	unsigned char *e = dst+len;
	int c;
	while(dst < e) {
		c = hval(*src);
		src++;
		c = c << 4;
		c |= hval(*src);
		src++;
		*dst = c;
		dst++;
	}
#undef hval
}

static void read_keys(const char *filename_prefix) {
	char buf[512];
	const struct fmap {
		char ext[5];
		unsigned char *dest;
		unsigned len;
	} map[] = {
		[0] = {	.ext = "pub",
			.dest = client_mode?static_keys.client.pubkey:static_keys.server.pubkey,
			.len = client_mode?sizeof(static_keys.client.pubkey):sizeof(static_keys.server.pubkey)
		},
		[1] = {	.ext = "priv",
			.dest = client_mode?static_keys.client.privkey:static_keys.server.privkey,
			.len = client_mode?sizeof(static_keys.client.privkey):sizeof(static_keys.server.privkey)
		},
		[2] = {.len = 0}
	};
	const struct fmap *curr;
	size_t got, want;
	for(curr = &map[0]; curr->len; ++curr) {
		snprintf(buf, sizeof buf, "%s.%s", filename_prefix, curr->ext);
		FILE *f = fopen(buf, "r");
		if(!f) {
			dolog("error opening %s\n", buf);
			exit(1);
		}
		want = curr->len*2;
		if(want != (got = fread(buf, 1, want, f))) {
			dolog("error: short read from keyfile, want %zu, got %zu\n", want, got);
			exit(1);
		}
		dehex(curr->dest, buf, curr->len);
		fclose(f);
	}
}

static void add_authorized_keys(char *ak) {
	/* for lazyness, since we know that SIGNPUBLICKEYBYTES == PUBLICKEYBYTES
	 * otherwise we'd need to make the list item size depend on whether
	 * it's client or server, and e.g. enforce use of -c as first parameter
	 * or move the add_... call out of the getopt() loop.  */
	assert(SIGNPUBLICKEYBYTES == PUBLICKEYBYTES);
	char *p = ak;
	size_t want = 2*PUBLICKEYBYTES;
	while(1) {
		if(strlen(ak) < want) {
			dolog("error: authorized key %s too short, expected %zu bytes\n", ak, want);
			exit(1);
		}
		unsigned char buf[PUBLICKEYBYTES];
		dehex(buf, p, PUBLICKEYBYTES);
		sblist_add(authorized_keys, buf);
		if((p = strchr(p, ':'))) ++p;
		else break;
	}
}

int main(int argc, char** argv) {
	int c;
	const char *listenip = "0.0.0.0", *bind_arg = 0, *key_loc = 0;
	unsigned port = 1080;
	timeout = 0;
	assert(SIGNPUBLICKEYBYTES == PUBLICKEYBYTES);
	authorized_keys = sblist_new(PUBLICKEYBYTES, 4);

	while((c = getopt(argc, argv, "cva:k:b:i:p:t:")) != -1) {
		switch(c) {
			case 'v':
				verbose++;
				break;
			case 'c':
				client_mode = 1;
				break;
			case 'a':
				add_authorized_keys(optarg);
				break;
			case 'k':
				key_loc = optarg;
				break;
			case 'b':
				bind_arg = optarg;
				break;
			case 'i':
				listenip = optarg;
				break;
			case 'p':
				port = atoi(optarg);
				break;
			case 't':
				timeout = atoi(optarg);
				break;
			case ':':
				dolog("error: option -%c requires an operand\n", optopt);
				/* fall-through */
			default:
				return usage();
		}
	}
	if(!argv[optind] || !key_loc)
		return usage();

	read_keys(key_loc);

	{
		char *p = strchr(argv[optind], ':');
		if(!p) {
			dolog("error: expected ip:port tuple\n");
			return usage();
		}
		*p = 0;
		int tport = atoi(p+1);
		struct addrinfo* remote;
		if(resolve(argv[optind], tport, &remote)) {
			*p = ':';
			dolog("error: cannot resolve %s\n", argv[optind]);
			return 1;
		}
		if(bind_arg) {
			if(resolve_sa(bind_arg, 0, &target.bind_addr))
				return complain_bind(bind_arg);
		} else
			SOCKADDR_UNION_AF(&target.bind_addr) = AF_UNSPEC;
		memcpy(&target.addr, remote->ai_addr, remote->ai_addrlen);
	}

	CRC32C_InitTables();

	signal(SIGPIPE, SIG_IGN);
	struct server s;
	sblist *threads = sblist_new(sizeof (struct thread*), 8);
	if(server_setup(&s, listenip, port)) {
		perror("server_setup");
		return 1;
	}
	server = &s;

	while(1) {
		collect(threads);
		struct client c;
		struct thread *curr = malloc(sizeof (struct thread));
		if(!curr) goto oom;
		curr->done = 0;
		if(server_waitclient(&s, &c)) continue;
		curr->client = c;
		if(!sblist_add(threads, &curr)) {
			close(curr->client.fd);
			free(curr);
			oom:
			dolog("rejecting connection due to OOM\n");
			usleep(16); /* prevent 100% CPU usage in OOM situation */
			continue;
		}
		pthread_attr_t *a = 0, attr;
		if(pthread_attr_init(&attr) == 0) {
			a = &attr;
			pthread_attr_setstacksize(a, THREAD_STACK_SIZE);
		}
		if(pthread_create(&curr->pt, a, clientthread, curr) != 0)
			dolog("pthread_create failed. OOM?\n");
		if(a) pthread_attr_destroy(&attr);
	}
}
